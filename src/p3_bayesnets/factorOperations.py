# factorOperations.py
# -------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from bayesNet import Factor
import operator as op
import util
import functools

def joinFactorsByVariableWithCallTracking(callTrackingList=None):


    def joinFactorsByVariable(factors, joinVariable):
        """
        Input factors is a list of factors.
        Input joinVariable is the variable to join on.

        This function performs a check that the variable that is being joined on 
        appears as an unconditioned variable in only one of the input factors.

        Then, it calls your joinFactors on all of the factors in factors that 
        contain that variable.

        Returns a tuple of 
        (factors not joined, resulting factor from joinFactors)
        """

        if not (callTrackingList is None):
            callTrackingList.append(('join', joinVariable))

        currentFactorsToJoin =    [factor for factor in factors if joinVariable in factor.variablesSet()]
        currentFactorsNotToJoin = [factor for factor in factors if joinVariable not in factor.variablesSet()]

        # typecheck portion
        numVariableOnLeft = len([factor for factor in currentFactorsToJoin if joinVariable in factor.unconditionedVariables()])
        if numVariableOnLeft > 1:
            print("Factor failed joinFactorsByVariable typecheck: ", factor)
            raise ValueError("The joinBy variable can only appear in one factor as an \nunconditioned variable. \n" +  
                               "joinVariable: " + str(joinVariable) + "\n" +
                               ", ".join(map(str, [factor.unconditionedVariables() for factor in currentFactorsToJoin])))
        
        joinedFactor = joinFactors(currentFactorsToJoin)
        return currentFactorsNotToJoin, joinedFactor

    return joinFactorsByVariable

joinFactorsByVariable = joinFactorsByVariableWithCallTracking()


def joinFactors(factors):
    """
    Question 3: Your join implementation 

    Input factors is a list of factors.  
    
    You should calculate the set of unconditioned variables and conditioned 
    variables for the join of those factors.

    Return a new factor that has those variables and whose probability entries 
    are product of the corresponding rows of the input factors.

    You may assume that the variableDomainsDict for all the input 
    factors are the same, since they come from the same BayesNet.

    joinFactors will only allow unconditionedVariables to appear in 
    one input factor (so their join is well defined).

    Hint: Factor methods that take an assignmentDict as input 
    (such as getProbability and setProbability) can handle 
    assignmentDicts that assign more variables than are in that factor.

    Useful functions:
    Factor.getAllPossibleAssignmentDicts
    Factor.getProbability
    Factor.setProbability
    Factor.unconditionedVariables
    Factor.conditionedVariables
    Factor.variableDomainsDict
    """

    # typecheck portion
    setsOfUnconditioned = [set(factor.unconditionedVariables()) for factor in factors]
    if len(factors) > 1:
        intersect = functools.reduce(lambda x, y: x & y, setsOfUnconditioned)
        if len(intersect) > 0:
            print("Factor failed joinFactors typecheck: ", factor)
            raise ValueError("unconditionedVariables can only appear in one factor. \n"
                    + "unconditionedVariables: " + str(intersect) + 
                    "\nappear in more than one input factor.\n" + 
                    "Input factors: \n" +
                    "\n".join(map(str, factors)))


    "*** YOUR CODE HERE ***"
    # Inicializar listas vacías
    no_condicionadas = []
    condicionadas = []
    dominios_variables = {}

    # Verificar si la lista de factores no está vacía
    if factors:
        # Obtener el primer factor en la lista
        dominios_variables = next(iter(factors)).variableDomainsDict()

    # Iterar sobre cada factor en la lista
    for factor in factors:
        # Obttener las variables no condicionadas y condicionadas para el factor actual
        aux_no_cond = factor.unconditionedVariables()
        aux_cond = factor.conditionedVariables()
        
        # Extender la lista de variables no condicionadas con variables que aún no están presentes
        no_condicionadas.extend([var for var in aux_no_cond if var not in no_condicionadas])
        
        # Igual que la sentencia anterior (ahora para condicionadas)
        condicionadas.extend([var for var in aux_cond if var not in condicionadas])

    # Eliminar variables de la lista de variables condicionadas si también están en la lista de variables no condicionadas
    condicionadas = [var for var in condicionadas if var not in no_condicionadas]

    # Crear un nuevo factor con las variables no condicionadas y condicionadas combinadas
    # utilizando el diccionario de dominios de variables del primer factor
    nuevo_factor = Factor(no_condicionadas, condicionadas, dominios_variables)

    # Obtener todas las asignaciones posibles para el nuevo factor
    todas_asignaciones = nuevo_factor.getAllPossibleAssignmentDicts()

    # Iterar sobre cada asignación y calcular la probabilidad conjunta multiplicando
    # las probabilidades de cada factor para esa asignación, luego establecer la probabilidad
    # en el nuevo factor
    for asignacion in todas_asignaciones:
        probabilidad = 1
        for factor in factors:
            probabilidad *= factor.getProbability(asignacion)
        nuevo_factor.setProbability(asignacion, probabilidad)

    # Devolver el nuevo factor que representa la distribución conjunta
    return nuevo_factor

    "*** END YOUR CODE HERE ***"


def eliminateWithCallTracking(callTrackingList=None):

    def eliminate(factor, eliminationVariable):
        """
        Question 4: Your eliminate implementation 

        Input factor is a single factor.
        Input eliminationVariable is the variable to eliminate from factor.
        eliminationVariable must be an unconditioned variable in factor.
        
        You should calculate the set of unconditioned variables and conditioned 
        variables for the factor obtained by eliminating the variable
        eliminationVariable.

        Return a new factor where all of the rows mentioning
        eliminationVariable are summed with rows that match
        assignments on the other variables.

        Useful functions:
        Factor.getAllPossibleAssignmentDicts
        Factor.getProbability
        Factor.setProbability
        Factor.unconditionedVariables
        Factor.conditionedVariables
        Factor.variableDomainsDict
        """
        # autograder tracking -- don't remove
        if not (callTrackingList is None):
            callTrackingList.append(('eliminate', eliminationVariable))

        # typecheck portion
        if eliminationVariable not in factor.unconditionedVariables():
            print("Factor failed eliminate typecheck: ", factor)
            raise ValueError("Elimination variable is not an unconditioned variable " \
                            + "in this factor\n" + 
                            "eliminationVariable: " + str(eliminationVariable) + \
                            "\nunconditionedVariables:" + str(factor.unconditionedVariables()))
        
        if len(factor.unconditionedVariables()) == 1:
            print("Factor failed eliminate typecheck: ", factor)
            raise ValueError("Factor has only one unconditioned variable, so you " \
                    + "can't eliminate \nthat variable.\n" + \
                    "eliminationVariable:" + str(eliminationVariable) + "\n" +\
                    "unconditionedVariables: " + str(factor.unconditionedVariables()))

        "*** YOUR CODE HERE ***"
        # Obtener las variables condicionadas y no condicionadas después de la eliminación
        new_unconditioned_vars = [var for var in factor.unconditionedVariables() if var != eliminationVariable]
        new_conditioned_vars = factor.conditionedVariables()

        # Crear un nuevo factor con las variables actualizadas
        new_factor = Factor(new_unconditioned_vars, new_conditioned_vars, factor.variableDomainsDict())

        # Calcular las sumas necesarias para las asignaciones que contienen la variable eliminada
        for assignment_dict in new_factor.getAllPossibleAssignmentDicts():
            sum_prob = 0.0
            for val in factor.variableDomainsDict()[eliminationVariable]:
                assignment_dict[eliminationVariable] = val
                sum_prob += factor.getProbability(assignment_dict)
            new_factor.setProbability(assignment_dict, sum_prob)

        return new_factor
        "*** END YOUR CODE HERE ***"

    return eliminate

eliminate = eliminateWithCallTracking()


def normalize(factor):
    """
    Question 5: Your normalize implementation 

    Input factor is a single factor.

    The set of conditioned variables for the normalized factor consists 
    of the input factor's conditioned variables as well as any of the 
    input factor's unconditioned variables with exactly one entry in their 
    domain.  Since there is only one entry in that variable's domain, we 
    can either assume it was assigned as evidence to have only one variable 
    in its domain, or it only had one entry in its domain to begin with.
    This blurs the distinction between evidence assignments and variables 
    with single value domains, but that is alright since we have to assign 
    variables that only have one value in their domain to that single value.

    Return a new factor where the sum of the all the probabilities in the table is 1.
    This should be a new factor, not a modification of this factor in place.

    If the sum of probabilities in the input factor is 0,
    you should return None.

    This is intended to be used at the end of a probabilistic inference query.
    Because of this, all variables that have more than one element in their 
    domain are assumed to be unconditioned.
    There are more general implementations of normalize, but we will only 
    implement this version.

    Useful functions:
    Factor.getAllPossibleAssignmentDicts
    Factor.getProbability
    Factor.setProbability
    Factor.unconditionedVariables
    Factor.conditionedVariables
    Factor.variableDomainsDict
    """

    # typecheck portion
    variableDomainsDict = factor.variableDomainsDict()
    for conditionedVariable in factor.conditionedVariables():
        if len(variableDomainsDict[conditionedVariable]) > 1:
            print("Factor failed normalize typecheck: ", factor)
            raise ValueError("The factor to be normalized must have only one " + \
                            "assignment of the \n" + "conditional variables, " + \
                            "so that total probability will sum to 1\n" + 
                            str(factor))

    "*** YOUR CODE HERE ***"
    # Obtener las variables condicionadas y no condicionadas
    new_unconditioned_vars = factor.unconditionedVariables()
    new_conditioned_vars = factor.conditionedVariables()

    # Encontrar variables no condicionadas con exactamente un valor en su dominio
    for var in factor.unconditionedVariables():
        if len(variableDomainsDict[var]) == 1:
            new_conditioned_vars.add(var)
            new_unconditioned_vars.remove(var)

    # Crear un nuevo factor con las variables actualizadas
    new_factor = Factor(new_unconditioned_vars, new_conditioned_vars, variableDomainsDict)

    # Calcular la suma total de probabilidades en el nuevo factor
    total_prob = sum([factor.getProbability(assignment_dict) for assignment_dict in new_factor.getAllPossibleAssignmentDicts()])

    # Normalizar las probabilidades dividiendo por la suma total
    if total_prob == 0:
        return None
    else:
        for assignment_dict in new_factor.getAllPossibleAssignmentDicts():
            prob = factor.getProbability(assignment_dict) / total_prob
            new_factor.setProbability(assignment_dict, prob)

    return new_factor
    "*** END YOUR CODE HERE ***"

